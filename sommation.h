// Six fonctions double/float décroissant/croissant/algo

double sommationDouble(int n) {
    double out = 0;
    for (int i = 1 ; i <= n ; i++) {
        out += 1.0 / i;
    }
    
    return out;
}

double sommationDoubleInv(int n) {
    double out = 0;
    for (int i = n ; i >= 1 ; i--) {
        out += 1.0 / i;
    }
    
    return out;
}

float sommationFloat(int n) {
    float out = 0;
    for (int i = 1 ; i <= n ; i++) {
        out += 1.0 / i;
    }
    
    return out;
}

float sommationFloatInv(int n) {
    float out = 0;
    for (int i = n ; i >= 1 ; i--) {
        out += 1.0 / i;
    }
    
    return out;
}

double sommationDoubleAutre(int n){
    double s = 1.0/1.0; // donc 1
    double c = 0; // c représente l’erreur à chaque addition
    double y;
    double t;
    for(int i = 2; i <= n; i++) {
        y = 1.0/i - c; //on additionne l’erreur de l’addition précédente au terme courant 
        t = s + y;
        c = (t - s) - y;
        s = t;
    }
    return s;
}

float sommationFloatAutre(int n){
    float s = 1.0/1.0; // donc 1
    float c = 0; // c représente l’erreur à chaque addition
    float y;
    float t;
    for(int i = 2; i <= n; i++) {
        y = 1.0/i - c; //on additionne l’erreur de l’addition précédente au terme courant 
        t = s + y;
        c = (t - s) - y;
        s = t;
    }
    return s;
}