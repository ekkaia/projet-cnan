#include <iostream>
#include <string.h>
#include <math.h>
#include "sommation.h"
#include "zeros_fonction.h"

using namespace std;

int main() {
    cout.precision(20); // 16 ?

    int values = 1;
    //cout << "Combien de valeurs voulez-vous ?" << endl;
    //cin >> values;
    values = 6;
    int sum = 10;
    for (int i = 1 ; i <= values ; i++) {
        
        //cout << "Quelle valeur pour sommation ?" << endl;
        //cin >> sum;
        
        cout << sum << endl;
        cout << "float       : " << sommationFloat(sum) << endl;       // recherché: 14.39272672286572363138
                                                                       // trouvé   : 14.357357978820800781 (1 chiffre après la virgule)
        cout << "floatInv    : " << sommationFloatInv(sum) << endl;    // recherché: 14.39272672286572363138
                                                                       // trouvé   : 14.392651557922363281 (3 chiffres après la virgule)
        cout << "floatAutre  : " << sommationFloatAutre(sum) << endl;  // recherché: 14.39272672286572363138
                                                                       // trouvé   : 14.392726898193359375 (6 chiffres après la virgule)  
        cout << "double      : " << sommationDouble(sum) << endl;      // recherché: 14.39272672286572363138
                                                                       // trouvé   : 14.392726722864781053 (11 chiffres après la virgule)
        cout << "doubleInv   : " << sommationDoubleInv(sum) << endl;   // recherché: 14.39272672286572363138
                                                                       // trouvé   : 14.392726722865756273 (13 chiffres après la virgule)
        cout << "doubleAutre : " << sommationDoubleAutre(sum) << endl; // recherché: 14.39272672286572363138
                                                                       // trouvé   : 14.392726722865724298 (14 chiffres après la virgule)        
        sum = sum*10;
    }

    
    cout << "Dichotomie :" << endl;
    cout << dichotomie(f1, -10.0, 10.0, 0.00001) << endl;
    cout << dichotomie(f2, -10.0, 10.0, 0.00001) << endl;
    cout << dichotomie(f3, -10.0, 10.0, 0.00001) << endl;
    cout << dichotomie(f4, -10.0, 10.0, 0.00001) << endl;
    cout << "Newton :" << endl;
    cout << newton(f1, 10.0, 0.00001) << endl;
    cout << newton(f2, 10.0, 0.00001) << endl;
    cout << newton(f3, 10.0, 0.00001) << endl;
    cout << newton(f4, 10.0, 0.00001) << endl;
    
    return 0;
}