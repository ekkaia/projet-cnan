double methodeSecante(double (*f)(double), double x1, double x0) {
    return ((*f)(x1) - (*f)(x0)) / (x1 - x0);
}

double newton(double (*f)(double), double x1, double precision) {
    double x0 = x1 + precision * 10;
    double secante = methodeSecante((*f), x1, x0);

    while(abs((*f)(x1)) > precision) {
        x1 = x0 - (*f)(x0) / secante;
        x0 = x1;
    }

    return x1;
}

/**
 * @brief Méthode par dichotomie
 * 
 * @param f Fonction dont on recherche le zéro
 * @param a Borne gauche de l'intervalle
 * @param b Borne droite de l'intervalle
 * @param precision Précision du résultat
 * @return double Retourne la racine (m)
 */
double dichotomie(double (*f)(double), double a, double b, double precision) {
    // Milieu
    double m;

    // Dichotomie
    while (b - a > precision) {
        m = (a + b) / 2;

        // Intervalle
        double fa = (*f)(a);
        double fm = (*f)(m);

        if (fa * fm <= 0) {
            b = m;
        } else {
            a = m;
        }
    }

    return m;
}

double f1(double x) {
    return (x*x*x) - 2*x - 5;
}

double f2(double x) {
    return exp(-x) - x;
}

double f3(double x) {
    return x*sin(x) - 1;
}

double f4(double x) {
    return (x*x*x) - 3*(x*x) + 3*x - 1;
}